import React from 'react';

import Properties from './Properties';
import classes from '../assets/PropertiesList.module.css';



const PropertiesList = (props) => {
  return (
    <ul className={classes['properties-list']}>
      {props.properties.map((props) => (
        <Properties
          key={props.id}
          codigo={props.codigo}
          tipo={props.tipo}
          locador={props.locador}
          nome={props.nome}
          imovel={props.imovel}
          endereco={props.endereco}
        />
      ))}
    </ul>
  );
};

export default PropertiesList;