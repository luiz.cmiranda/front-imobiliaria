import React, { useRef } from "react";
import {ToastContainer} from "react-toastify"
import "react-toastify/dist/ReactToastify.css"
import classes from "../assets/AddProperties.module.css";

import {toast} from 'react-toastify'

function AddProperties(props) {
  const tipoRef = useRef("");
  const codigoRef = useRef("");
  const nomeRef = useRef("");
  const locadorRef = useRef("");
  const imovelRef = useRef("");
  const enderecoRef = useRef("");

  function submitHandler(event) {
    event.preventDefault();
    console.log(tipoRef);
    const list = {
      tipo: tipoRef.current.value,
      codigo: codigoRef.current.value,
      locador: locadorRef.current.value,
      nome: nomeRef.current.value,
      imovel: imovelRef.current.value,
      endereco: enderecoRef.current.value,
    };

    props.onAddAdresse(list);
    toast.success(`Adicionado com sucesso!!`)
    document.forms[0].reset();
    
  }

  return (
    <form onSubmit={submitHandler}>
      <div className={classes.control}>
        <label htmlFor="tipo">Tipo de Locação</label>
        <input type="text" id="tipo" ref={tipoRef} />
      </div>
      <div className={classes.control}>
        <label htmlFor="codigo">Codigo Unico</label>
        <input type="number" id="codigo" ref={codigoRef} />
      </div>
      <div className={classes.control}>
        <label htmlFor="locador">Codigo Locador</label>
        <input type="number" id="locador" ref={locadorRef} />
      </div>
      <div className={classes.control}>
        <label htmlFor="nome">Nome Locador</label>
        <input type="text" id="nome" ref={nomeRef} />
      </div>

      <div className={classes.control}>
        <label htmlFor="imovel">Codigo Imovel</label>
        <input type="number" id="imovel" ref={imovelRef} />
      </div>
      <div className={classes.control}>
        <label htmlFor="endereco">Endereço Imovel</label>
        <textarea type="text" id="endereco" ref={enderecoRef} />
      </div>
      <button >Adicionar</button>
      <ToastContainer theme="colored" position="top-center" />
    </form>
  );
}

export default AddProperties;
