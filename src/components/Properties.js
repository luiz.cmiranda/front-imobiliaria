import React from 'react';

import classes from '../assets/Properties.module.css';



const deleteData = async (id) =>{
  const response = await fetch( `http://localhost:1337/catalogos/${id}`, {
      method: 'DELETE', 
      headers: {
        'Content-Type': 'application/json'
      },
  });

 const data = await response.json();
  console.log(data);
};


const Properties = (props) => {
  return (
    <li className={classes.properties}>
      <h2>{props.tipo}</h2>
      <h3>Codigo Unico: {props.codigo}</h3>
      <p>Codigo Locador: {props.locador}</p>
      <p>Nome Locador: {props.nome}</p>
      <p>Codigo Imovel: {props.imovel}</p>
      <p>Endereço Imovel:  {props.endereco}</p>
      <button onClick={() =>{deleteData()}}>Deletar</button>
    </li>
  );
};

export default Properties ;
