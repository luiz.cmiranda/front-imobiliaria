import React, { useState, useEffect, useCallback } from "react";

import AddProperties from "./components/AddProperties";
import PropertiesList from "./components/PropertiesList";
import "./App.css";

function App() {
  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const fetchAdresseHandler = useCallback(async () => {
    setIsLoading(true);
    const response = await fetch("http://localhost:1337/catalogos");
    const data = await response.json();

    const loadedPropierts = [];
    for (const key in data) {
      loadedPropierts.push({
        id: key,
        tipo: data[key].tipo,
        codigo: data[key].codigo,
        locador: data[key].locador,
        nome: data[key].nome,
        imovel: data[key].imovel,
        endereco: data[key].endereco,
      });
    }

    setList(loadedPropierts);
    setIsLoading(false);
  }, []);

  useEffect(() => {
    fetchAdresseHandler();
  }, [fetchAdresseHandler]);

  async function addAdresseHandler(list) {
    const response = await fetch("http://localhost:1337/catalogos", {
      method: "POST",
      body: JSON.stringify(list),
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    console.log(data);
  }

  return (
    <React.Fragment>
      <section>
        <AddProperties onAddAdresse={addAdresseHandler} />
      </section>
      <section>
        <button onClick={fetchAdresseHandler}>Imoveis Disponiveis</button>
      </section>
      <section>
        {!isLoading && list.length > 0 && <PropertiesList properties={list} />}
        {!isLoading && list.length === 0 && <p>Click no botão para exibir</p>}
        {isLoading && <p>Carregando...</p>}
      </section>
    </React.Fragment>
  );
}

export default App;

